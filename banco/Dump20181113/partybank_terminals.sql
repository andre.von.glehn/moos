CREATE DATABASE  IF NOT EXISTS `partybank` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `partybank`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: partybank
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `terminals`
--

DROP TABLE IF EXISTS `terminals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `terminal_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `evento_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `sync_menu` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `bloqueado` tinyint(1) NOT NULL DEFAULT '0',
  `caixa_fechado` tinyint(1) NOT NULL DEFAULT '0',
  `caixa_conferido` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'informa se a diferença de caixa já foi paga ou se o caixa fechou zerado',
  `valor_apurado` double(10,2) DEFAULT '0.00',
  `observacao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `terminals_evento_id_foreign` (`evento_id`),
  KEY `terminals_user_id_foreign` (`user_id`),
  CONSTRAINT `terminals_evento_id_foreign` FOREIGN KEY (`evento_id`) REFERENCES `eventos` (`id`),
  CONSTRAINT `terminals_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terminals`
--

LOCK TABLES `terminals` WRITE;
/*!40000 ALTER TABLE `terminals` DISABLE KEYS */;
INSERT INTO `terminals` VALUES (2,'d65fb8cc',42,61,1,'2018-03-09 19:15:08','2018-05-16 14:36:34',0,0,0,0,0.00,NULL),(4,'d65fb8cd',41,61,0,'2018-04-26 19:44:08','2018-05-28 16:35:31',0,0,1,1,0.00,NULL),(5,'d65fb8cc',42,61,0,'2018-05-16 14:36:34','2018-05-16 14:37:03',0,0,0,0,0.00,NULL),(6,'d65fb8cc',42,61,0,'2018-05-16 14:37:03','2018-05-16 14:37:58',0,0,0,0,0.00,NULL),(7,'d65fb8cc',42,61,1,'2018-05-16 14:37:58','2018-06-11 12:47:08',0,0,0,0,0.00,NULL),(9,'d65fb8cd',41,62,1,'2018-05-16 20:03:45','2018-05-28 18:32:19',1,0,1,1,2000.00,'sei nao'),(10,'d65fb8cc',42,4,1,'2018-06-11 12:47:08','2018-06-12 01:10:16',0,0,0,0,0.00,NULL),(11,'d65fb8cc',42,4,1,'2018-06-12 01:10:16','2018-06-12 01:10:59',0,0,0,0,0.00,NULL),(12,'d65fb8cc',42,3,0,'2018-06-12 01:10:59','2018-06-12 01:11:00',0,0,0,0,0.00,NULL),(13,'d65fb8cc',42,3,1,'2018-06-12 01:11:00','2018-06-12 01:11:51',0,0,0,0,0.00,NULL),(14,'d65fb8cc',41,61,1,'2018-06-12 01:11:51','2018-06-12 01:11:55',1,0,0,0,0.00,NULL);
/*!40000 ALTER TABLE `terminals` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-13 23:23:26
